package restfull.restfull_messenger.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import restfull.restfull_messenger.database.DataBases;
import restfull.restfull_messenger.model.Message;
import restfull.restfull_messenger.model.Profile;

public class ProfileService {
	private Map<String, Profile> profiles = DataBases.getProfiles();

	//Default Constructor
	public ProfileService() {
		profiles.put("1", new Profile(1, "AAA", "AA", "AA"));
	}
	
	//return all profiles...
	public List<Profile> getAllProfiles() {
		return new ArrayList<Profile>(profiles.values());
	}
	
	//return profile which have a given id.
	public Profile getProfiles(String profileName) {
		return profiles.get(profileName);
	}
	
	//add a new profile
	public Profile addProfile(Profile profile) {
		profile.setId(profiles.size() + 1);
		profiles.put(profile.getProfileName(), profile);
		
		return profile;
	}
	
	//update given profile
	public Profile updateProfile(Profile profile) {
		if(profile.getProfileName().isEmpty()) {
			return null;
		}
		profiles.put(profile.getProfileName(), profile);
		
		return profile;
	}
	
	//remove given profile
	public Profile removeProfile(String profileName) {
		return profiles.remove(profileName);
	}

	
	

}
