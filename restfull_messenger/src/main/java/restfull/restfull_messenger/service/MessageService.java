package restfull.restfull_messenger.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import restfull.restfull_messenger.customException.DataNotFoundException;
import restfull.restfull_messenger.database.DataBases;
import restfull.restfull_messenger.model.Message;

public class MessageService {
	private Map<Integer, Message> messages = DataBases.getMessages();

	// return all messages...
	public List<Message> getAllMessages() {

		return new ArrayList(messages.values());
	}

	// return message which have a given id.
	public Message getMessage(int id) {
		Message message = messages.get(id);
		if(message == null) {
			throw new DataNotFoundException("Message with id " + id + " not fount");
		}
		
		return message;
	}

	// add a new message
	public Message addMessage(Message message) {
		message.setId(messages.size() + 1);
		messages.put(message.getId(), message);

		return message;
	}

	public Message updateMessage(Message message) {
		if (message.getId() <= 0) {
			return null;
		}
		messages.put(message.getId(), message);

		return message;
	}

	public Message removeMessage(int id) {
		return messages.remove(id);
	}

	// for filtering
	public List<Message> getAllMessagesForYear(int year) {
		List<Message> messageForYear = new ArrayList();
		Calendar cal = Calendar.getInstance();
		for (Message message : messages.values()) {
			cal.setTime(message.getCreated());
			if (cal.get(Calendar.YEAR) == year) {
				messageForYear.add(message);
			}
		}
		return messageForYear;
	}
	

	public List<Message> getAllMessagesPaginated(int start, int size) {
		ArrayList<Message> list = new ArrayList<>(messages.values());
		if(start + size > list.size()) {
			return new ArrayList<Message>();
		}
       return list.subList(start, start + size);
	}

}
