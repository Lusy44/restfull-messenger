package restfull.restfull_messenger.database;

import java.util.HashMap;
import java.util.Map;

import restfull.restfull_messenger.model.Message;
import restfull.restfull_messenger.model.Profile;

public class DataBases {
	private static Map<Integer, Message> messages = new HashMap<>();
    private static Map<String, Profile> profiles = new HashMap<>();
	
    /**
     * return all messages
     * @return
     */
	public static Map<Integer, Message> getMessages() {
		return messages;
	}
	
	/**
	 * return all profiles
	 * @return
	 */
	public static Map<String, Profile> getProfiles(){
		return profiles;
	}

}

