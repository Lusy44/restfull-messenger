package restfull.restfull_messenger.customException;

public class DataNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 5174062093328376668L;

    //Default constructor
	public DataNotFoundException(String message) {
		super(message);
	}

}
