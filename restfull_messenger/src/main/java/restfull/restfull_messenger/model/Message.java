package restfull.restfull_messenger.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Message {

	private int id;
	private String message;
	private Date created;
	private String author;
	
	private List<Link> links = new ArrayList<>();
	
	//Default constructor
	public Message() {
		
	}
	
	//Constructor by arguments
	public Message(int id, String message, String author) {
		this.id = id;
		this.message = message;
		this.author = author;
		this.created = new Date();
	}
	
	public void addLink(String url, String rel) {
		Link link = new Link();
		
		link.setLink(url);
		link.setRel(rel);
		links.add(link);
		
	}
	

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the links
	 */
	public List<Link> getLinks() {
		return links;
	}

	/**
	 * @param links the links to set
	 */
	public void setLinks(List<Link> links) {
		this.links = links;
	}
	
}
