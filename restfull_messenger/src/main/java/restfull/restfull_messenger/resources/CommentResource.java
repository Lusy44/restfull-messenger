package restfull.restfull_messenger.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

//SubResource for a messanger....  using @Path("/") syntax for a subPath
@Path("/")
public class CommentResource {
	
	@GET
	public String getAllComments() {
		return "new sub resource...  messages/messageId/comments";
	}
	
	@GET
	@Path("/{commentId}")
	//messageId value to get in parent class when call this method
	public String getComentByGivenId(@PathParam("messageId") int messageId, 
			                         @PathParam("commentId") int commentId) {
		System.out.println("MessageId = " + messageId + " CommentId = " + commentId);
		
		return "Method to return...  messages/messageId/comments/commentId";
	}

}
