package restfull.restfull_messenger.resources;

import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import restfull.restfull_messenger.model.Message;
import restfull.restfull_messenger.resources.beans.MessageFilterBean;
import restfull.restfull_messenger.service.MessageService;

//root path for all methods (/messages)
@Path("/messages")
//for put method format
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MessageResource {
	MessageService messageService = new MessageService();
	
	
//	@GET
//	public List<Message> getMessages(@QueryParam("year") int year,
//			                         @QueryParam("start") int start,
//			                         @QueryParam("size") int size) {
//		if(year > 0) {
//			return messageService.getAllMessagesForYear(year);
//		}
//		
//		if(start > 0 && size > 0) {
//			return messageService.getAllMessagesPaginated(start, size);
//		}
//		
//		return messageService.getAllMessages();
//	}
	
	
	
	// We can use up method by @QueryParam or can use this method with @BeanParam
	@GET
	public List<Message> getMessages(@BeanParam MessageFilterBean filterBean) {
		if(filterBean.getYear() > 0) {
			return messageService.getAllMessagesForYear(filterBean.getYear());
		}
		
		if(filterBean.getStart() > 0 && filterBean.getSize() > 0) {
			return messageService.getAllMessagesPaginated(filterBean.getStart(), filterBean.getSize());
		}
		
		return messageService.getAllMessages();
	}
	
	//use root path + method path("/messages/test")
	@GET
	@Path("/{messageId}")
	//@PathParam... give the path for the method parameter
	public Message getMessage(@PathParam("messageId") int id, @Context UriInfo uriInfo) {
		Message message = messageService.getMessage(id);
		message.addLink(getUriForSelf(uriInfo, message), "self");
		
		return message;
	}
	
	
	@POST
	public Message addMessage(Message message) {
		
		return messageService.addMessage(message);
	}
	
	
	@PUT
	@Path("/{messageId}")
	//@PathParam given because don't create new message other update message
	public Message updateMessage(@PathParam("messageId") int id, Message message) {
		message.setId(id);
		return messageService.updateMessage(message);
	}
	
	@DELETE
	@Path("/{messageId}")
	public void deleteMessage(@PathParam("messageId") int id) {
		messageService.removeMessage(id);
	}
	
	//using SubResource for a messange comment
	@Path("/{messageId}/comments")
	public CommentResource getCommentResource() {
		return new CommentResource();
	}
	
	//return uri.... which json can show url(link).
	private String getUriForSelf(UriInfo uriInfo, Message message) {
		String uri = uriInfo.getBaseUriBuilder()
				     .path(MessageResource.class)                //http://localhost:8080/messenger/webapi/
				     .path(Integer.toString(message.getId()))                                        //messages
				     .build()                                                                           //{messageId}
				     .toString();
				     
	   return uri;
	}
	
	
}
